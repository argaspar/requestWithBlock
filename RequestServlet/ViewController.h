//
//  ViewController.h
//  RequestServlet
//
//  Created by Rafael Gaspar on 22/11/17.
//  Copyright © 2017 Rafael Gaspar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)lanzarPeticion:(id)sender;
@property (strong, nonatomic) IBOutlet UITextView *parametrosText;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *indicadorView;

@end


//
//  Service.h
//  RequestServlet
//
//  Created by Rafael Gaspar on 23/11/17.
//  Copyright © 2017 Rafael Gaspar. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^ServiceConnectionDictionaryBlock) (NSDictionary *result);
typedef void (^ServiceConnectionErrorBlock) (NSError *error);

@interface Service : NSObject

-(void)retrieveLoginWithCompletionBlock:(NSDictionary*)parameters completion:(ServiceConnectionDictionaryBlock)onCompletion errorBlock:(ServiceConnectionErrorBlock)onError;
- (BOOL)isNetworkAvailable;
@end

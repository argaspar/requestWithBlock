//
//  ViewController.m
//  RequestServlet
//
//  Created by Rafael Gaspar on 22/11/17.
//  Copyright © 2017 Rafael Gaspar. All rights reserved.
//

#import "ViewController.h"
#import "Service.h"

@interface ViewController ()

@end

@implementation ViewController{
    Service *requestService;
    NSDictionary *parametros;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    requestService = [[Service alloc]init];
    [self.indicadorView setHidden:YES];
    
    parametros = @{@"origen": @"pibee",
                   @"idioma": @"es",
                   @"eai_user": @"0026009434678885ADMIN2",
                   @"empresa": @"34678885",
                   @"cod_emp": @"xxxx8885",
                   @"cod_usu": @"ADMIN2",
                   @"eai_password": @"ZAQ1XSW2"};
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}


-(void) showAlert:(UIViewController *)context withTitle:(NSString *)title Message:(NSString *)message{
    
    UIAlertController * alert = [UIAlertController
                                 alertControllerWithTitle: title
                                 message: message
                                 preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                               actionWithTitle:@"Aceptar"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction * action) {
                                   
                               }];
    
    [alert addAction:okButton];
    [context presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)lanzarPeticion:(id)sender {
    
    [self.indicadorView setHidden:NO];
    
    if([requestService isNetworkAvailable]){
        
        [_parametrosText setText:[NSString stringWithFormat:@"%@",parametros]];
        [self.indicadorView startAnimating];
        
        [requestService retrieveLoginWithCompletionBlock:parametros completion:^(NSDictionary *respuesta){
            [self showAlert:self withTitle:@"Respuesta del servidor" Message:[NSString stringWithFormat:@"%@",respuesta]];
            
        } errorBlock:^(NSError *error){
            [self showAlert:self withTitle:@"Respuesta del servidor" Message:[NSString stringWithFormat:@"%@",error]];
            [self.indicadorView setHidden:YES];
            [self.indicadorView stopAnimating];
            
        }];
        
    }else{
        [self showAlert:self withTitle:@"Aplicacion" Message:@"Compruebe conexion a internet"];
         [self.indicadorView stopAnimating];
    }
}
@end

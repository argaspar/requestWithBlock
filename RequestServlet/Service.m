//
//  Service.m
//  RequestServlet
//
//  Created by Rafael Gaspar on 23/11/17.
//  Copyright © 2017 Rafael Gaspar. All rights reserved.
// 
#import "Service.h"
#import <SystemConfiguration/SystemConfiguration.h>

#define SITIO_WEB "https://150.250.238.46/DFAUTH/slod_mx_web/DFServlet"

@implementation Service


-(void)retrieveLoginWithCompletionBlock:(NSDictionary*)parameters completion:(ServiceConnectionDictionaryBlock)onCompletion errorBlock:(ServiceConnectionErrorBlock)onError{
    
    NSError *error;
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:nil];
    NSURL *URL = [NSURL URLWithString:@"https://150.250.238.46/DFAUTH/slod_mx_web/DFServlet"];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:URL
                                                        cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPMethod:@"POST"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:&error];
    
    [request setHTTPBody:postData];
    NSLog(@"URL: %@",request);
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        
        if(data != nil){
            
            id jsonData = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
            NSDictionary *responseDict = jsonData;
            NSLog(@"Respuesta: %@",jsonData);
            onCompletion(responseDict);
        }else{
            onError(error);
        }
    }];
    
    [postDataTask resume];
    
}

- (BOOL)isNetworkAvailable{
    
    //VALIDAR CONEXION A INTERNET
    SCNetworkReachabilityRef referencia = SCNetworkReachabilityCreateWithName (kCFAllocatorDefault, SITIO_WEB);
    SCNetworkReachabilityFlags resultado;
    SCNetworkReachabilityGetFlags (referencia, &resultado);
    
    CFRelease(referencia);
    
    if (resultado & kSCNetworkReachabilityFlagsReachable) {
        return YES;
    }else{
        return NO;
    }
}
@end

//
//  AppDelegate.h
//  RequestServlet
//
//  Created by Rafael Gaspar on 22/11/17.
//  Copyright © 2017 Rafael Gaspar. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

